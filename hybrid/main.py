import sys
import serial
from PySide import QtGui, QtCore

# ser = serial.Serial('/dev/ttyACM0', 9600)


class Example(QtGui.QWidget):

    def __init__(self):
        super(Example, self).__init__()
        self.initUI()
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.updateNums)
        timer.start(1000)
        self.updateNums()

    def initUI(self):

        self.big = QtGui.QHBoxLayout()

        self.left = QtGui.QVBoxLayout()
        self.leftText = QtGui.QLCDNumber(self)
        self.leftText.setDigitCount(6)

        self.leftTitle = QtGui.QLabel("Battery Level")
        self.leftTitle.setAlignment(QtCore.Qt.AlignCenter)
        self.leftTitle.setFixedHeight(15)

        self.left.addWidget(self.leftTitle)
        self.left.addWidget(self.leftText)

        self.right = QtGui.QVBoxLayout()
        self.rightText = QtGui.QLCDNumber(self)
        self.rightText.setDigitCount(6)

        self.rightTitle = QtGui.QLabel("Current")
        self.rightTitle.setAlignment(QtCore.Qt.AlignCenter)
        self.rightTitle.setFixedHeight(15)

        self.right.addWidget(self.rightTitle)
        self.right.addWidget(self.rightText)

        self.big.addLayout(self.left)
        self.big.addLayout(self.right)

        self.setGeometry(30, 30, 800, 480)
        self.setWindowTitle('SAE Dash')

        self.setLayout(self.big)

        self.show()

    def updateNums(self):

        # serial_line = str(ser.readline())
        # final = "".join(e for e in serial_line if e in "1234567890.,").split(",")
        # try:
        #     first, second = float(final[0]), float(final[1])
        # except Exception:
        #     first, second = 0, 0
        # print(first, second)
        # self.leftText.display(str(first))
        # self.rightText.display(str(second))
        # self.update()
        self.leftText.display(str(0))
        self.rightText.display(str(0))


def main():
    app = QtGui.QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()

ser.close()