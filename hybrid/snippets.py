import serial

ser = serial.Serial('/dev/ttyACM0', 9600)

while True:
    serial_line = str(ser.readline())
    final = "".join(e for e in serial_line if e in "1234567890.,").split(",")
    try:
        first, second = float(final[0]), float(final[1])
    except Exception:
        first, second = 0, 0

    print(first, second)


ser.close()
